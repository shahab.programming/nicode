import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task1/Controllers/navigation_controller.dart';
import 'package:task1/Controllers/public_controller.dart';
import 'package:task1/GetXController/get_public_controller.dart';
import 'package:task1/Models/public_model.dart';
import 'package:task1/endpoints.dart';

class AuthController {
  final PublicController _publicController = PublicController();
  Future<void> sendSms(String phoneNumber, {bool? isResend}) async {
    PublicModel _res = await _publicController.postReq(
      endPoint: EndPoints.signin,
      body: {'mobile': phoneNumber},
      showLoading: true,
    );
    if (!_res.error && _res.data['statusCode'] == '200') {
      if (isResend == true) {
        return;
      }
      GetPublicController _getPublicController = Get.find();
      _getPublicController.phoneNumber = phoneNumber;

      NavigationController.smsConfirmation();
    }
  }

  Future<void> verifation(String phoneNumber, String token) async {
    PublicModel _res = await _publicController.postReq(
        endPoint: EndPoints.verification,
        body: {'mobile': phoneNumber, 'activeCode': token},
        showLoading: true);
    if (!_res.error && _res.data['id'] != null) {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString('UserId', _res.data['id']);
      NavigationController.home(isOff: true);
    }
  }
}
