import 'package:task1/Controllers/public_controller.dart';
import 'package:task1/Models/approved_model.dart';
import 'package:task1/Models/public_model.dart';
import 'package:task1/endpoints.dart';

class HomeController {
  final PublicController _publicController = PublicController();

  Future<List<ApprovedModel>>? getApproveds() async {
    PublicModel _res = await _publicController.getReq(
      endPoint: EndPoints.approved,
    );
    final List<ApprovedModel> _list = [];

    if (!_res.error && _res.data['statusCode'] == 200) {
      var _data = _res.data['data'];
      for (var item in _data) {
        List<String> _images = [];
        for (var _image in item['gallery']) {
          _images.add(_image);
        }
        final ApprovedModel _model = ApprovedModel(
          title: item['title'],
          firstName: item['userAccount']['firstName'],
          lastName: item['userAccount']['lastName'],
          amountRequired: item['amountRequired'].toString(),
          investorsCount: item['investorsCount'].toString(),
          remainingDays: item['remainingDays'].toString(),
          payedAmounts: item['payedAmounts'].toString(),
          images: _images,
        );
        _list.add(_model);
      }
    }
    return _list;
  }
}
