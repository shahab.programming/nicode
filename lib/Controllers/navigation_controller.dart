import 'package:get/route_manager.dart';
import 'package:task1/Views/Home/home.dart';
import 'package:task1/Views/Login/login.dart';
import 'package:task1/Views/SmsConfirmation/sms_confirmation.dart';

class NavigationController {
  static login({isOff = false}) {
    if (isOff) return Get.off(() => const Login());
    return Get.to(() => const Login());
  }

  static smsConfirmation({isOff = false}) {
    if (isOff) return Get.off(() => const SmsConfirmation());
    return Get.to(() => const SmsConfirmation());
  }

  static home({isOff = false}) {
    if (isOff) return Get.off(() => const Home());
    return Get.to(() => const Home());
  }
}
