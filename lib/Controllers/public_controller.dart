import 'dart:convert';

import 'package:dio/dio.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:task1/Models/public_model.dart';

import '../endpoints.dart';

class PublicController {
  final apiKey = "67e91f79-wqe2be8-4a4b-a40b0s-dcsdweqsdqwef78";

  getReq(
      {required String endPoint,
      Map<String, dynamic>? body,
      bool showApiMessage = false,
      String? method,
      bool showLoading = false}) async {
    http.Dio dio = http.Dio();
    http.CancelToken cancelToken = http.CancelToken();

    dio.options.baseUrl = EndPoints.baseRoute;
    dio.options.connectTimeout = 10000;
    dio.options.receiveTimeout = 10000;

    try {
      if (showLoading) {
        Get.focusScope?.unfocus();

        Get.dialog(
          WillPopScope(
            onWillPop: () async {
              cancelToken.cancel();
              return true;
            },
            child: Center(
              child: CircularProgressIndicator(
                color: Get.theme.primaryColor,
              ),
            ),
          ),
        );
      }

      http.Response data = await dio.get(
        endPoint,
        cancelToken: cancelToken,
        queryParameters: body,
        options: http.Options(
          method: 'GET',
          responseType: http.ResponseType.plain,
        ),
      );

      if (data.statusCode! >= 200 && data.statusCode! < 300) {
        if (data.data.isNotEmpty) {
          var jsonData = json.decode(data.data.toString());

          if (jsonData['message'] != null &&
              jsonData['message'].toString().isNotEmpty &&
              showApiMessage) {
            await Get.showSnackbar(GetBar(
              duration: const Duration(seconds: 2),
              message: jsonData['message'],
            ));
          }
          if (Get.isDialogOpen == true) {
            Get.back();
          }
          return PublicModel(data: jsonData);
        }
        if (Get.isDialogOpen == true) {
          Get.back();
        }
        return const PublicModel(error: true);
      }
      if (Get.isDialogOpen == true) {
        Get.back();
      }
      return PublicModel(error: true, errorMessage: data.data);
    } on Exception catch (e) {
      // ignore: avoid_print
      print(e);
      if (Get.isDialogOpen == true) {
        Get.back();
      }
      return const PublicModel(error: true);
      // /return postReq(endPoint: endPoint, body: body);
    }
  }

  Future<PublicModel> postReq(
      {required String endPoint,
      required body,
      bool showApiMessage = false,
      String? method,
      bool showLoading = false}) async {
    http.Dio dio = http.Dio();
    http.CancelToken cancelToken = http.CancelToken();

    dio.options.baseUrl = EndPoints.baseRoute;
    dio.options.connectTimeout = 10000;
    dio.options.receiveTimeout = 10000;

    try {
      if (showLoading) {
        Get.focusScope?.unfocus();

        Get.dialog(
          WillPopScope(
            onWillPop: () async {
              cancelToken.cancel();
              return true;
            },
            child: Center(
              child: CircularProgressIndicator(
                color: Get.theme.primaryColor,
              ),
            ),
          ),
        );
      }

      http.Response data = await dio.post(
        endPoint,
        cancelToken: cancelToken,
        data: body,
        options: http.Options(
          method: 'POST',
          contentType: http.Headers.jsonContentType,
          responseType: http.ResponseType.plain,
        ),
      );

      if (data.statusCode! >= 200 && data.statusCode! < 300) {
        if (data.data.isNotEmpty) {
          Map<String, String> jsonData = json.decode(data.data.toString());

          if (jsonData['message'] != null &&
              jsonData['message'].toString().isNotEmpty &&
              showApiMessage) {
            await Get.showSnackbar(GetBar(
              duration: const Duration(seconds: 2),
              message: jsonData['message'],
            ));
          }
          if (Get.isDialogOpen == true) {
            Get.back();
          }
          return PublicModel(data: jsonData);
        }
        if (Get.isDialogOpen == true) {
          Get.back();
        }
        return const PublicModel(error: true);
      }
      if (Get.isDialogOpen == true) {
        Get.back();
      }
      return PublicModel(error: true, errorMessage: data.data);
    } on Exception catch (e) {
      // ignore: avoid_print
      print(e);
      if (Get.isDialogOpen == true) {
        Get.back();
      }
      return const PublicModel(error: true);
      // /return postReq(endPoint: endPoint, body: body);
    }
  }

  Map<String, String> textToData(String? text) {
    Map<String, String> _res = {};
    if (text == null || text.isEmpty) return _res;
    text = text.substring(0, text.length - 1);
    text = text.substring(1);
    List<String> _keyVals = text.split(', ');
    for (String _item in _keyVals) {
      List<String> _splited = _item.split(": ");
      _res[_splited[0]] = _splited[1];
    }

    return _res;
  }
}
