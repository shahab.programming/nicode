import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Styles {
  static const TextStyle placeholder =
      TextStyle(color: Colors.grey, fontSize: 12);
  static const TextStyle link = TextStyle(color: Colors.blue, fontSize: 12);
  static const TextStyle normal = TextStyle(color: Colors.black, fontSize: 12);
}
