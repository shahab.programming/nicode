import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  final double height;
  final VoidCallback onTap;
  const Footer({Key? key, required this.height, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    return SizedBox(
      height: height,
      child: Stack(
        clipBehavior: Clip.antiAlias,
        children: [
          CustomPaint(
            size: Size(_size.width, height),
            painter: const MyPainter(),
          ),
          Positioned(
            bottom: 25,
            // left: ,
            left: _size.width / 2 - 25,
            child: GestureDetector(
              onTap: onTap,
              child: Container(
                padding: const EdgeInsets.only(left: 3),
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                    color: Colors.white, shape: BoxShape.circle),
                child: const Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.blue,
                  textDirection: TextDirection.ltr,
                ),
              ),
            ),
          ),
        ],
      ),
    );

    //   return CustomPaint(
    //     size: Size(double.infinity, 60),
    //     painter: BottomNavigationMenuCustomPainter(),
    //   );
    // }
  }
}

class MyPainter extends CustomPainter {
  const MyPainter();
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    Path path = Path();
    paint.color = Colors.blue;
    path = Path();
    path.moveTo(0, size.height);
    path.lineTo(0, size.height * 0.6);
    path.cubicTo(
        0, size.height * 0.6, size.width / 2, 0, size.width, size.height * 0.6);
    path.lineTo(size.width, size.height);
    canvas.drawShadow(path.shift(const Offset(0, 3)), Colors.black12, 5, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
