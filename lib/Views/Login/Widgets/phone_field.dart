import 'package:flutter/material.dart';

import '../../../styles.dart';

class PhoneField extends StatelessWidget {
  const PhoneField({
    Key? key,
    required TextEditingController controller,
  })  : _controller = controller,
        super(key: key);

  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.number,
      maxLength: 10,
      controller: _controller,
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
      decoration: InputDecoration(
        hintText: '9** *** ****',
        hintTextDirection: TextDirection.ltr,
        hintStyle: Styles.placeholder,
        suffixIcon: const Icon(
          Icons.phone_android,
          color: Colors.grey,
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade50),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
