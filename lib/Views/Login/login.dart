import 'package:flutter/material.dart';
import 'package:task1/Controllers/auth_controller.dart';
import 'package:task1/Views/Widgets/fullpage_with_header.dart';
import 'package:task1/Views/Widgets/header.dart';
import 'package:task1/Views/Login/Widgets/phone_field.dart';
import 'package:task1/constants.dart';
import 'package:task1/styles.dart';

import 'Widgets/footer.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _controller = TextEditingController();
  @override
  @override
  Widget build(BuildContext context) {
    return FullPageWithHeader(
      children: [
        const Header(),
        const SizedBox(height: 30),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: RichText(
            textAlign: TextAlign.start,
            text: const TextSpan(
              style: Styles.normal,
              text: 'لطفا برای ',
              children: [
                TextSpan(
                  text: " ورود ",
                  style: Styles.link,
                ),
                TextSpan(
                  text: "یا",
                ),
                TextSpan(
                  text: " عضویت ",
                  style: Styles.link,
                ),
                TextSpan(
                  text: "شماره همراه خود را وارد کنید",
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 30),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 35),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: PhoneField(controller: _controller),
              ),
              const SizedBox(width: 20),
              Image.asset(
                "${Constants.IMAGE_FOLDER}ir.png",
                height: 20,
              ),
              const Text("+98"),
            ],
          ),
        ),
        const Spacer(),
        Footer(
          height: 150,
          onTap: () {
            if (_controller.text.length < 10) return;

            AuthController _authController = AuthController();
            _authController.sendSms(_controller.text);
          },
        ),
      ],
    );
  }
}
