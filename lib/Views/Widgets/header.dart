import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomPaint(
          size: Size(MediaQuery.of(context).size.width, 250),
          painter: const MyPainter(),
        ),
        const Positioned(
          left: 0,
          bottom: 50,
          top: 0,
          right: 0,
          child: Icon(
            Icons.ac_unit,
            color: Colors.black,
            size: 50,
          ),
        ),
      ],
    );

    //   return CustomPaint(
    //     size: Size(double.infinity, 60),
    //     painter: BottomNavigationMenuCustomPainter(),
    //   );
    // }
  }
}

class MyPainter extends CustomPainter {
  const MyPainter();
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    Path path = Path();
    paint.color = Colors.white;
    path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height * 0.6);
    path.cubicTo(0, size.height * 0.6, size.width / 2, size.height, size.width,
        size.height * 0.6);
    path.lineTo(size.width, 0);
    canvas.drawShadow(path.shift(const Offset(0, 3)), Colors.black12, 5, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
