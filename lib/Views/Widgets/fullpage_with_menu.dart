import 'package:flutter/material.dart';
import 'package:task1/Views/Widgets/bottom_nav.dart';

class FullPagewithMenu extends StatelessWidget {
  const FullPagewithMenu({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            child: child,
          ),
          const Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: BottomNav(),
          ),
        ],
      )),
    );
  }
}
