import 'package:get/get.dart';
import 'package:task1/GetXController/get_public_controller.dart';
import 'package:flutter/material.dart';

class NavButton extends StatelessWidget {
  final int index;
  final IconData? icon;
  const NavButton({
    Key? key,
    required this.index,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GetX<GetPublicController>(builder: (controller) {
        return GestureDetector(
          onTap: () {
            controller.mainPageIndex = index;
          },
          child: Container(
            color: Colors.transparent,
            child: Icon(
              icon ?? Icons.person,
              color: controller.mainPageIndex == index
                  ? Colors.blue
                  : Colors.black,
            ),
          ),
        );
      }),
    );
  }
}
