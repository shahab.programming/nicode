import 'package:flutter/material.dart';
import 'package:task1/Views/Widgets/nav_button.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        CustomPaint(
          size: Size(size.width, 90),
          painter: const MyPainter(),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          top: 40,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: const [
              NavButton(
                index: 1,
                icon: Icons.home,
              ),
              NavButton(
                index: 2,
                icon: Icons.access_alarm_outlined,
              ),
              SizedBox(width: 80),
              NavButton(
                index: 3,
                icon: Icons.search,
              ),
              NavButton(
                index: 4,
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 10,
          left: size.width / 2 - 25,
          child: Container(
            decoration:
                const BoxDecoration(color: Colors.blue, shape: BoxShape.circle),
            child: const Center(
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 30,
              ),
            ),
            width: 50,
            height: 50,
          ),
        ),
      ],
    );

    //   return CustomPaint(
    //     size: Size(double.infinity, 60),
    //     painter: BottomNavigationMenuCustomPainter(),
    //   );
    // }
  }
}

class MyPainter extends CustomPainter {
  const MyPainter();
  @override
  void paint(Canvas canvas, Size size) {
    //size.height is bottom of screen
    Paint paint = Paint();
    Path path = Path();
    paint.color = Colors.white;
    path = Path();
    path.moveTo(0, size.height);
    path.lineTo(0, size.height - 50);
    path.lineTo(size.width / 2 - 40, size.height - 50);
    path.cubicTo(size.width / 2 - 40, size.height - 50, size.width / 2,
        size.height - 90, size.width / 2 + 40, size.height - 50);
    path.lineTo(size.width, size.height - 50);

    path.lineTo(size.width, size.height);
    canvas.drawShadow(path.shift(const Offset(0, -3)), Colors.black12, 5, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
