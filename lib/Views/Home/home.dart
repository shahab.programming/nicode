import 'package:persian_number_utility/persian_number_utility.dart';

import 'package:flutter/material.dart';
import 'package:task1/Controllers/home_controller.dart';
import 'package:task1/Models/approved_model.dart';
import 'package:task1/Views/Widgets/fullpage_with_menu.dart';
import 'package:task1/styles.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    final HomeController _homeController = HomeController();
    return FullPagewithMenu(
      child: FutureBuilder(
        future: _homeController.getApproveds(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            List<ApprovedModel> _data = snapshot.data!;
            return ListView.builder(
              itemCount: _data.length,
              itemBuilder: (BuildContext context, int index) {
                ApprovedModel _model = _data[index];
                return Container(
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.all(10),
                  width: _size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: const [
                      BoxShadow(
                        blurRadius: 5,
                        color: Colors.black12,
                        offset: Offset(0, 3),
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              _model.title,
                              style: Styles.normal.copyWith(fontSize: 12),
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Container(
                        height: 150,
                        width: _size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Colors.grey, width: 2),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: PageView.builder(
                            itemCount: _model.images.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Image.network(
                                _model.images[index],
                                width: _size.width,
                                height: 150,
                                fit: BoxFit.fill,
                              );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          const Icon(Icons.warning_amber),
                          const SizedBox(width: 10),
                          Text(
                            '${_model.firstName} ${_model.lastName}',
                            style: Styles.normal,
                          ),
                          const Spacer(),
                          Text(
                            _model.amountRequired.toPersianDigit().seRagham(),
                            style: Styles.link.copyWith(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            ' تومان ',
                            style: Styles.normal,
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        height: 100,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: BlueContainer(
                                title: 'زمان باقی مانده',
                                icon: Icons.access_alarm,
                                value:
                                    "${_model.remainingDays.toPersianDigit()}روز",
                              ),
                            ),
                            const SizedBox(width: 5),
                            Expanded(
                              flex: 2,
                              child: BlueContainer(
                                icon: Icons.person,
                                title: 'تعداد سرمایه بازار',
                                value: _model.investorsCount.toPersianDigit(),
                              ),
                            ),
                            const SizedBox(width: 5),
                            Expanded(
                              flex: 2,
                              child: BlueContainer(
                                value:
                                    "${_model.payedAmounts.toPersianDigit().seRagham()}تومان",
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class BlueContainer extends StatelessWidget {
  final IconData? icon;
  final String? title, value;
  const BlueContainer({
    Key? key,
    this.icon,
    this.title,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.blue.shade100,
      ),
      child: Column(
        children: [
          Icon(icon ?? Icons.crop_square),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  title ?? 'سرمایه گذاری تا الان',
                  textAlign: TextAlign.center,
                  style: Styles.placeholder.copyWith(fontSize: 10),
                ),
              ),
            ],
          ),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  value ?? 'سرمایه گذاری تا الان',
                  textAlign: TextAlign.center,
                  style: Styles.normal.copyWith(fontSize: 15),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
