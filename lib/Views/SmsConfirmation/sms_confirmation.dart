import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:task1/Controllers/auth_controller.dart';
import 'package:task1/GetXController/get_public_controller.dart';
import 'package:task1/Views/Widgets/fullpage_with_header.dart';
import 'package:task1/Views/Widgets/header.dart';
import 'package:task1/styles.dart';

import 'Widgets/timer_text.dart';

class SmsConfirmation extends StatelessWidget {
  const SmsConfirmation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GetPublicController _getPublicController = Get.find();
    return FullPageWithHeader(
      children: [
        const Header(),
        Text(
          'تایید شماره همراه',
          textAlign: TextAlign.center,
          style:
              Styles.link.copyWith(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: RichText(
            text: TextSpan(
              text: ' کد چهار رقمی ارسال شده به شماره ',
              style: Styles.placeholder,
              children: [
                TextSpan(
                    text: _getPublicController.phoneNumber, style: Styles.link),
                const TextSpan(
                  text: ' را وارد کنید ',
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 60),
          child: PinFieldAutoFill(
              decoration: const BoxLooseDecoration(
                  strokeColorBuilder: FixedColorBuilder(Colors.black)),
              onCodeChanged: (code) {
                if (code!.length == 4) {
                  FocusScope.of(context).requestFocus(FocusNode());
                  AuthController _authController = AuthController();
                  _authController.verifation(
                      _getPublicController.phoneNumber, code);
                }
              },
              // onCodeSubmitted: (code) {
              //   if (code.length < 4) return;
              //   FocusScope.of(context).requestFocus(FocusNode());

              //   AuthController _authController = AuthController();
              //   _authController.verifation(
              //       _getPublicController.phoneNumber, code);
              // },
              codeLength: 4),
        ),
        const SizedBox(height: 20),
        const TimerText()
      ],
    );
  }
}
