import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task1/Controllers/auth_controller.dart';
import 'package:task1/GetXController/get_public_controller.dart';
import 'package:task1/styles.dart';

class TimerText extends StatefulWidget {
  const TimerText({Key? key}) : super(key: key);

  @override
  _TimerTextState createState() => _TimerTextState();
}

class _TimerTextState extends State<TimerText> {
  late final RxInt _timerCounter;
  @override
  void initState() {
    _timerCounter = 120.obs;
    _startCountDown();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (_timerCounter.value < 1) {
        return GestureDetector(
          onTap: () {
            AuthController _authcontroller = AuthController();
            GetPublicController _getPublicController = Get.find();
            _timerCounter.value = 120;
            _startCountDown();

            _authcontroller.sendSms(_getPublicController.phoneNumber);
          },
          child: const Text(
            'برای ارسال دوباره کد ضربه بزنید',
            style: Styles.link,
            textAlign: TextAlign.center,
          ),
        );
      }
      return Text(
        '${_timerCounter.value} ثانیه دیگر باقی مانده',
        textAlign: TextAlign.center,
        style: Styles.link,
      );
    });
  }

  _startCountDown() async {
    if (_timerCounter.value < 1) {
      return;
    }
    await Future.delayed(const Duration(seconds: 1));
    _timerCounter.value--;
    _startCountDown();
  }
}
