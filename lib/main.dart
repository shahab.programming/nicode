import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task1/Views/Home/home.dart';
import 'package:task1/Views/Login/login.dart';

import 'GetXController/get_public_controller.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences _prefs = await SharedPreferences.getInstance();
  bool _logedIn = false;
  if (_prefs.containsKey('UserId')) {
    _logedIn = true;
  }
  runApp(
    MyApp(
      hasLoged: _logedIn,
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.hasLoged}) : super(key: key);
  final bool hasLoged;
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: DetailsBinding(),
      debugShowCheckedModeBanner: false,
      home: hasLoged ? const Home() : const Login(),
      localizationsDelegates: const [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale("fa", "IR"),
      ],
      locale: const Locale("fa", "IR"),
    );
  }
}

class DetailsBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<GetPublicController>(GetPublicController());
  }
}
