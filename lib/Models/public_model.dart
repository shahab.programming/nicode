class PublicModel {
  final dynamic data;
  final bool error;
  final String? errorMessage;

  const PublicModel({this.data, this.error = false, this.errorMessage});
}
