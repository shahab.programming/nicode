class ApprovedModel {
  final String title,
      firstName,
      lastName,
      amountRequired,
      investorsCount,
      remainingDays,
      payedAmounts;
  final List<String> images;

  ApprovedModel({
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.amountRequired,
    required this.investorsCount,
    required this.remainingDays,
    required this.payedAmounts,
    required this.images,
  });
}
