import 'package:get/get.dart';

class GetPublicController extends GetxController {
  final RxInt _mainPageIndex = 1.obs;
  String phoneNumber = '';

  int get mainPageIndex => _mainPageIndex.value;
  set mainPageIndex(int myValue) {
    _mainPageIndex.value = myValue;
  }
}
