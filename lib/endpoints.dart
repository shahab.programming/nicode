class EndPoints {
  static const String baseRoute = "https://appgatewaytech.kargozareman.com/v1/";
  static const String signin = "signin";
  static const String verification = "signin/verification";
  static const String approved = "crowds/proposals/approved";
}
